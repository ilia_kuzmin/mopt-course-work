import copy
import matplotlib.pyplot as plt
from solver import balas_algorithm, brute_force, vectors_are_equal, scalar_product
import numpy as np

# def transform_solution(solution):
#     transformed = np.zeros(7)
#
#     transformed[0] = 1 - solution[4]
#     transformed[1] = 1 - solution[1]
#     transformed[2] = 1 - solution[2]
#     transformed[3] = 1 - solution[5]
#     transformed[4] = 1 - solution[6]
#     transformed[5] = 1 - solution[3]
#     transformed[6] = 1 - solution[0]
#
#     return transformed
#
#
# def main():
#     costs = np.array([7000, 8000, 10000, 12000, 16000, 19500, 22000])
#     constraints_m = np.array([[0, 0, -1, 0, 1, 0, 0],
#                               [0, 1, -1, 0, 0, 0, 0],
#                               [0, 0, 0, 0, 1, -1, 0],
#                               [0, 1, 0, 0, 0, -1, 0],
#                               [3000, 2500, 3500, 4500, 5000, 6000, 7000]])
#     constraints_v = np.array([0, 0, 0, 0, 16500])
#     solution_bl = balas_algorithm(costs, constraints_m, constraints_v)
#     solution_br = brute_force(costs, constraints_m, constraints_v)
#     print(solution_bl)
#     print(transform_solution(solution_bl))


# zone of combat san'check


def sensitivity():
    cost_function = [-16000, -8000, -10000, -19500, -22000, -12000, -7000]
    constraint_matrix = [[-1, 0, 1, 0, 0, 0, 0],
                         [0, -1, 1, 0, 0, 0, 0],
                         [-1, 0, 0, 1, 0, 0, 0],
                         [0, -1, 0, 1, 0, 0, 0],
                         [-5000, -2500, -3500, -6000, -7000, -4500, -3000]]
    constraint_vector = [0, 0, 0, 0, -15000]
    grid_for_investments = [range(-4500, 3000, 500), range(-2000, 5500, 500),
                            range(-3000, 4500, 500), range(-5500, 2000, 500)]
    grid_for_profit = [range(-15000, 10000, 500), range(-7000, 10000, 500),
                       range(-9500, 10000, 500), range(-18500, 10000, 500)]

    for number_of_argument in range(4):
        solutions = []
        arguments = []
        values = []
        previous_solution = None

        for current_add in grid_for_investments[number_of_argument]:
            tmp = copy.deepcopy(constraint_matrix)
            tmp[4][number_of_argument] = -(-tmp[4][number_of_argument] + current_add)
            current_solution = brute_force(cost_function, tmp, constraint_vector)

            # if not vectors_are_equal(previous_solution, current_solution):
            solutions.append(current_solution)
            arguments.append(-tmp[4][number_of_argument])
            values.append(-scalar_product(current_solution, cost_function))
            # previous_solution = current_solution

        fig, ax = plt.subplots(figsize=(12, 6))
        ax.scatter(arguments, values)
        ax.set(xlabel='инвестиции', ylabel='общая прибыль')
        plt.xticks(np.arange(min(arguments), max(arguments) + 500, 500))
        fig.savefig('plots/' + 'investment' + str(number_of_argument + 1) + '.png')

        solutions = []
        arguments = []
        values = []
        solution_argument = []
        solution_value = []
        previous_solution = None

        for current_add in grid_for_profit[number_of_argument]:
            tmp = copy.copy(cost_function)
            tmp[number_of_argument] = -(-tmp[number_of_argument] + current_add)
            current_solution = brute_force(tmp, constraint_matrix, constraint_vector)
            previous_solution = current_solution if previous_solution is None else previous_solution
            solutions.append(current_solution)
            arguments.append(-tmp[number_of_argument])
            values.append(-scalar_product(current_solution, tmp))

            if not vectors_are_equal(previous_solution, current_solution):
                solution_argument.append(-tmp[number_of_argument])
                solution_value.append(-scalar_product(current_solution, tmp))
                previous_solution = current_solution

        fig1, ax = plt.subplots(figsize=(12, 6))
        ax.scatter(arguments, values)
        ax.scatter(solution_argument, solution_value)
        ax.set(xlabel='прибыль', ylabel='общая прибыль')
        plt.xticks(np.arange(min(arguments), max(arguments) + 2000, 2000))
        fig1.savefig('plots/' + 'profit' + str(number_of_argument + 1) + '.png')


def main():
    sensitivity()


# zone of combat san'check


if __name__ == '__main__':
    main()
